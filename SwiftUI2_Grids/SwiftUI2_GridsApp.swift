////  SwiftUI2_GridsApp.swift
//  SwiftUI2_Grids
//
//  Created on 16/02/2021.
//  
//

import SwiftUI

@main
struct SwiftUI2_GridsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
