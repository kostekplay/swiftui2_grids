////  ContentView.swift
//  SwiftUI2_Grids
//
//  Created on 16/02/2021.
//  
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Grid5()
    }
}

struct Grid5: View {
    
    let data = Bundle.main.decode([String].self, from: "data.json")
    
    let layout = [
        GridItem(.adaptive(minimum: 80), spacing: 8, alignment: .center)
    ]
    
    var body: some View {
        ScrollView (/*@START_MENU_TOKEN@*/.vertical/*@END_MENU_TOKEN@*/, showsIndicators: false, content: {
            LazyVGrid (columns: layout, alignment: .center, spacing: 8, pinnedViews: /*@START_MENU_TOKEN@*/[]/*@END_MENU_TOKEN@*/, content: {
                ForEach(data, id: \.self) { item in
                    
                    VStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, content: {
                        Image(systemName: item)
                            .padding(.top,4)
                            .font(.subheadline)
                        Capsule()
                            .fill(Color.blue)
                            //.fill( Int(item) % 2 == 0 ? Color.blue : Color.black)
                            .frame(height: 16)
                            .padding(.horizontal,4)
                            .padding(.bottom, 4)
                    })
                    .overlay(
                        RoundedRectangle(cornerRadius: 8)
                            .stroke(Color.blue, lineWidth: 2)
                    )
                    
                }
            })
            .padding(.horizontal,6)
        })
    }
}

struct Grid4: View {
    
    
    func jsonTwo() -> [String]{
            let url = Bundle.main.url(forResource: "data", withExtension: "json")!
            let data = try! Data(contentsOf: url)
            let decoder = JSONDecoder()
            let products = try? decoder.decode([String].self, from: data)
            return products!
    }
    
    let layout = [
        GridItem(.adaptive(minimum: 80), spacing: 8, alignment: .center)
    ]
    
    var body: some View {
        
        ScrollView (/*@START_MENU_TOKEN@*/.vertical/*@END_MENU_TOKEN@*/, showsIndicators: false, content: {
            LazyVGrid (columns: layout, alignment: .center, spacing: 8, pinnedViews: /*@START_MENU_TOKEN@*/[]/*@END_MENU_TOKEN@*/, content: {
                ForEach(jsonTwo(), id: \.self) { item in
                    
                    VStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, content: {
                        Image(systemName: item)
                        Capsule()
                            .fill(Color.blue)
                            //.fill( Int(item) % 2 == 0 ? Color.blue : Color.black)
                            .frame(height: 16)
                            .padding(.horizontal,4)
                            .padding(.bottom, 4)
                    })
                    .overlay(
                        RoundedRectangle(cornerRadius: 8)
                            .stroke(Color.blue, lineWidth: 2)
                    )
                    
                }
            })
            .padding(.horizontal,6)
        })
    }
}

struct Grid3: View {
    
    let data = Array(1...1000).map {"Item \($0)"}
    
    let layout = [
        GridItem(.adaptive(minimum: 80), spacing: 8, alignment: .center)
    ]
    
    var body: some View {
        ScrollView (.horizontal, showsIndicators: false, content: {
            LazyHGrid (rows: layout, alignment: .center, spacing: 8, pinnedViews: /*@START_MENU_TOKEN@*/[]/*@END_MENU_TOKEN@*/, content: {
                ForEach(data, id: \.self) { item in
                    
                    VStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, content: {
                        Text(item)
                            .padding(.top,4)
                            .font(.subheadline)
                        Capsule()
                            .fill(Color.blue)
                            //.fill( Int(item) % 2 == 0 ? Color.blue : Color.black)
                            .frame(height: 16)
                            .padding(.horizontal,4)
                            .padding(.bottom, 4)
                    })
                    .overlay(
                        RoundedRectangle(cornerRadius: 8)
                            .stroke(Color.blue, lineWidth: 2)
                    )
                    
                }
            })
            .padding(.horizontal,6)
        })
    }
}

struct Grid2: View {
    
    let data = Array(1...1000).map {"Item \($0)"}
    
    let layout = [
        GridItem(.flexible(), spacing: 8, alignment: .center),
        GridItem(.flexible(), spacing: 8, alignment: .center),
        GridItem(.flexible(), spacing: 8, alignment: .center)
    ]
    
    var body: some View {
        ScrollView (/*@START_MENU_TOKEN@*/.vertical/*@END_MENU_TOKEN@*/, showsIndicators: false, content: {
            LazyVGrid (columns: layout, alignment: .center, spacing: 8, pinnedViews: /*@START_MENU_TOKEN@*/[]/*@END_MENU_TOKEN@*/, content: {
                ForEach(data, id: \.self) { item in
                    
                    VStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, content: {
                        Text(item)
                            .padding(.top,4)
                            .font(.subheadline)
                        Capsule()
                            .fill(Color.blue)
                            //.fill( Int(item) % 2 == 0 ? Color.blue : Color.black)
                            .frame(height: 16)
                            .padding(.horizontal,4)
                            .padding(.bottom, 4)
                    })
                    .overlay(
                        RoundedRectangle(cornerRadius: 8)
                            .stroke(Color.blue, lineWidth: 2)
                    )
                    
                }
            })
            .padding(.horizontal,6)
        })
    }
}

struct Grid1: View {
    
    let data = Array(1...1000).map {"Item \($0)"}
    
    let layout = [
        GridItem(.adaptive(minimum: 80), spacing: 8, alignment: .center)
    ]
    
    var body: some View {
        ScrollView (/*@START_MENU_TOKEN@*/.vertical/*@END_MENU_TOKEN@*/, showsIndicators: false, content: {
            LazyVGrid (columns: layout, alignment: .center, spacing: 8, pinnedViews: /*@START_MENU_TOKEN@*/[]/*@END_MENU_TOKEN@*/, content: {
                ForEach(data, id: \.self) { item in
                    
                    VStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, content: {
                        Text(item)
                            .padding(.top,4)
                            .font(.subheadline)
                        Capsule()
                            .fill(Color.blue)
                            //.fill( Int(item) % 2 == 0 ? Color.blue : Color.black)
                            .frame(height: 16)
                            .padding(.horizontal,4)
                            .padding(.bottom, 4)
                    })
                    .overlay(
                        RoundedRectangle(cornerRadius: 8)
                            .stroke(Color.blue, lineWidth: 2)
                    )
                    
                }
            })
            .padding(.horizontal,6)
        })
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

extension Bundle {
    func decode<T: Decodable>(_ type: T.Type, from file: String, dateDecodingStrategy: JSONDecoder.DateDecodingStrategy = .deferredToDate, keyDecodingStrategy: JSONDecoder.KeyDecodingStrategy = .useDefaultKeys) -> T {
        guard let url = self.url(forResource: file, withExtension: nil) else {
            fatalError("Failed to locate \(file) in bundle.")
        }

        guard let data = try? Data(contentsOf: url) else {
            fatalError("Failed to load \(file) from bundle.")
        }

        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = dateDecodingStrategy
        decoder.keyDecodingStrategy = keyDecodingStrategy

        do {
            return try decoder.decode(T.self, from: data)
        } catch DecodingError.keyNotFound(let key, let context) {
            fatalError("Failed to decode \(file) from bundle due to missing key '\(key.stringValue)' not found – \(context.debugDescription)")
        } catch DecodingError.typeMismatch(_, let context) {
            fatalError("Failed to decode \(file) from bundle due to type mismatch – \(context.debugDescription)")
        } catch DecodingError.valueNotFound(let type, let context) {
            fatalError("Failed to decode \(file) from bundle due to missing \(type) value – \(context.debugDescription)")
        } catch DecodingError.dataCorrupted(_) {
            fatalError("Failed to decode \(file) from bundle because it appears to be invalid JSON")
        } catch {
            fatalError("Failed to decode \(file) from bundle: \(error.localizedDescription)")
        }
    }
}
